var gulp = require('gulp'),
    sass = require('gulp-sass'),
    clean = require('gulp-clean'),
    jsmin = require("gulp-uglify"),
    sourcemaps = require('gulp-sourcemaps'),
    jade = require('gulp-jade'),
    autoprefixer = require('gulp-autoprefixer');
 
///*Clean*/
gulp.task('clean', function(){
    return gulp.src("./dist/css/*.css")
        .pipe(clean());
});


// sass
gulp.task('sass-dev', ['clean'], function() {
    return gulp.src('./src/sass/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(autoprefixer())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./dist/css/'));
});

gulp.task('sass-production', ['clean'], function() {
    return gulp.src('./src/sass/**/*.scss')
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(autoprefixer())
        .pipe(gulp.dest('./dist/css/'));
});

///*COPY FONTS*/
gulp.task('copy-fonts', function(){
    gulp.src('src/fonts/**/*.{ttf,woff,woff2,eot,svg}')
        .pipe(gulp.dest('dist/fonts/'));
});

///*JADE TASK*/
gulp.task('jade', function() {
    return gulp.src("./src/jade/*.jade")
        .pipe(jade({pretty: true}))
        .pipe(gulp.dest('./'))
});

///*JS-MIN TASK*/
gulp.task("js-min", function() {
    return gulp.src("./src/js/*.js")
        .pipe(jsmin())
        .pipe(gulp.dest("./dist/js/"))
});

///*WATCH TASK*/
gulp.task("watch", function (){
    gulp.watch('./src/sass/**/**/*.scss',   ['sass-production']);
    gulp.watch('./src/jade/**/*.jade',   ['jade']);
    gulp.watch('./src/js/**/*.js',       ['js-min']);
});

gulp.task("default", ['sass-production', 'jade'], function(){});
