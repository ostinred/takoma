$(document).ready(function () {
    var mySwiper = new Swiper ('.swiper-container', {
        loop: true,
        pagination: '.swiper-pagination',
        paginationClickable: true,
        autoplay: 3000,
        slidesPerView: 1
    });

    $('ul.tabs').each(function(){
        var $active, $content, $links = $(this).find('a');
        $active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);
        $active.addClass('active');
        $content = $($active[0].hash);
        $links.not($active).each(function () {
            $(this.hash).hide();
        });
        $(this).on('click', 'a', function(e){
            $active.removeClass('active');
            $content.hide();
            $active = $(this);
            $content = $(this.hash);
            $active.addClass('active');
            $content.show();
            e.preventDefault();
        });
    });
});


 map
function initialize() {
    var latlng = new google.maps.LatLng(50.393797, 30.489785);
    var myOptions = {
        zoom: 15,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    //var image = {
    //    url: './../img/marker.png',
    //    size: new google.maps.Size(42, 60)
    //};
    if(document.getElementById("map")){
        var map = new google.maps.Map(document.getElementById("map"), myOptions);
        var beachMarker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: 'ул. Васильковская, 30'
            //icon: image
        });
    }
}
google.maps.event.addDomListener(window, "load", initialize);